// Client side C/C++ program to demonstrate Socket programming
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define PORT 4444
#define ADDRESS "127.0.0.1"
#define LENGTH 30
#define SIZE_PACKET 1024
#define SPECIAL_CHARACTER ','
#define LOG_IN '1'
#define SEARCH_STUDENT '2'
#define SEARCH_SUBJECT '3'
#define SEARCH_GRADE '4'
#define LOG_OUT '5'
#define OK "OK"
#define EXIT_SOCKET "exit"
#define ALL "ALL"
#define NO_DATA "NO DATA DISPLAY"

int socketEventData(char *data, char hello[]);
void dataLogin(char *result, char acc[], char pass[]);
void runGame(int clientSocket);
void login(int clientSocket);
void mainMenu(int clientSocket);
void searchStudent(int clientSocket);
void detectDataStudent(char buffer[]);
void searchSubject(int clientSocket);
void detectDataSubject(char buffer[]);
void searchGrade(int clientSocket);
void detectDataGrade(char buffer[]);
void clrscr();

int main(int argc, char const *argv[])
{
  clrscr();
  int clientSocket, ret;
  struct sockaddr_in serverAddr;
  char buffer[SIZE_PACKET];

  clientSocket = socket(AF_INET, SOCK_STREAM, 0);
  if (clientSocket < 0)
  {
    printf("[-]Error in connection.\n");
    exit(1);
  }
  printf("[+]Client Socket is created.\n");

  memset(&serverAddr, '\0', sizeof(serverAddr));
  serverAddr.sin_family = AF_INET;
  serverAddr.sin_port = htons(PORT);
  serverAddr.sin_addr.s_addr = inet_addr(ADDRESS);

  ret = connect(clientSocket, (struct sockaddr *)&serverAddr, sizeof(serverAddr));
  if (ret < 0)
  {
    printf("[-]Error in connection.\n");
    exit(1);
  }
  printf("[+]Connected to Server.\n");
  runGame(clientSocket);

  return 0;
}

void runGame(int clientSocket)
{
  while (1)
  {
    char option;
    printf("Choose option: \n");
    printf("1. Login\n");
    printf("2. Exit\n");
    printf("Enter your choice: ");
    scanf(" %c", &option);
    if (option == '1')
    {
      login(clientSocket);
      mainMenu(clientSocket);
      continue;
    }
    else if (option == '2')
    {
      close(clientSocket);
      printf("[-]Disconnected from server.\n");
      exit(1);
      break;
    }
    else
    {
      clrscr();
      continue;
    }
  }
}

void login(int clientSocket)
{
  bool check = true;
  char mssv[LENGTH] = "", password[LENGTH] = "";
  char data[SIZE_PACKET];
  int dodai;

  while (check)
  {
    printf("Hello world. Please login to use service\n");
    printf("Enter MSSV: ");
    fflush(stdin);
    scanf("%s", mssv);
    printf("Enter password: ");
    fflush(stdin);
    scanf("%s", password);
    char data_login[strlen(mssv) + strlen(password) + 1];
    dataLogin(data_login, mssv, password);
    send(clientSocket, data_login, strlen(data_login), 0);
    if (recv(clientSocket, data, SIZE_PACKET, 0) < 0)
    {
      printf("[-]Error in receiving data.\n");
    }
    else
    {
      if (data[0] == '1')
      {
        printf("Login success!\n");
        check = false;
      }
      else
      {
        printf("Account or Password wrong. Please try again\n");
      }
    }
  }
}

void dataLogin(char *result, char acc[], char pass[])
{
  result[0] = '1';
  int dem = strlen(acc) + 1;
  for (int i = 0; i < strlen(acc); i++)
  {
    result[i + 1] = acc[i];
  }
  result[dem] = SPECIAL_CHARACTER;
  dem += 1;
  for (int i = 0; i < strlen(pass); i++)
  {
    result[dem + i] = pass[i];
  }
  result[dem + strlen(pass)] = SPECIAL_CHARACTER;
}

void mainMenu(int clientSocket)
{
  clrscr();
  bool check = true;
  char *s = "====================";
  char s_logout[SIZE_PACKET];
  while (check)
  {
    printf("%s> Menu <%s\n", s, s);
    printf("1. Search infor Student\n");
    printf("2. Search infor Subject\n");
    printf("3. Search infor Grade\n");
    printf("4. Logout\n");
    printf("Choose the option: ");
    char option;
    scanf(" %c", &option);
    switch (option)
    {
    case '1':
      searchStudent(clientSocket);
      break;
    case '2':
      searchSubject(clientSocket);
      break;
    case '3':
      searchGrade(clientSocket);
      break;
    case '4':
      s_logout[0] = LOG_OUT;
      s_logout[1] = SPECIAL_CHARACTER;
      send(clientSocket, s_logout, strlen(s_logout), 0);
      if (recv(clientSocket, s_logout, SIZE_PACKET, 0) < 0)
      {
        printf("[-]Error in receiving data.\n");
      }
      else
      {
        if (s_logout[0] == OK[0] && s_logout[1] == OK[1])
        {
          clrscr();
          check = false;
        }
        else
        {
          printf("[-] Error server");
        }
      }
      break;
    default:
      break;
    }
  }
}

void searchStudent(int clientSocket)
{
  clrscr();
  char *s = "====================";
  char buffer[SIZE_PACKET];
  char recvBuf[SIZE_PACKET];
  char mssv[LENGTH];
  char option = 0;
  printf("%s> Search Student <%s", s, s);
  printf("\nEnter MSSV: ");
  fflush(stdin);
  scanf("%s", mssv);
  buffer[0] = SEARCH_STUDENT;
  buffer[1] = SPECIAL_CHARACTER;
  for (int i = 0; i < strlen(mssv); i++)
  {
    buffer[i + 2] = mssv[i];
  }
  buffer[strlen(mssv) + 2] = SPECIAL_CHARACTER;
  send(clientSocket, buffer, strlen(buffer), 0);
  remove(buffer);
  if (recv(clientSocket, recvBuf, SIZE_PACKET, 0) < 0)
  {
    printf("[-]Error in receiving data.\n");
  }
  else
  {
    printf("%s>Result:<%s\n", s, s);
    detectDataStudent(recvBuf);
    remove(recvBuf);
  }
}

void detectDataStudent(char buffer[])
{
  if (buffer[0] == NO_DATA[0] && buffer[1] == NO_DATA[1])
  {
    printf("%s\n", NO_DATA);
  }
  else
  {
    int dem = 0;
    printf("MSSV: ");
    for (int i = 0; i < strlen(buffer); i++)
    {
      if (buffer[i] == SPECIAL_CHARACTER)
      {
        dem = dem + 1;
        switch (dem)
        {
        case 2:
          printf("\nFull Name: ");
          break;
        case 3:
          printf("\nDate of Birthday: ");
          break;
        case 4:
          printf("\nSex: ");
          break;
        case 5:
          printf("\nClass: ");
          break;
        case 6:
          printf("\nEmail: ");
          break;
        case 7:
          printf("\nPhone: ");
          break;
        default:
          break;
        }
      }
      else
      {
        if (dem != 1)
          printf("%c", buffer[i]);
        if (dem > 7)
          break;
      }
    }
    printf("\n");
  }
}

void searchSubject(int clientSocket)
{
  clrscr();
  char *s = "====================";
  char buffer[SIZE_PACKET];
  char recvBuf[SIZE_PACKET];
  char mssv[LENGTH];
  char option = 0;
  printf("%s> Search Subject <%s", s, s);
  printf("\nEnter SubjectID: ");
  fflush(stdin);
  scanf("%s", mssv);
  buffer[0] = SEARCH_SUBJECT;
  buffer[1] = SPECIAL_CHARACTER;
  for (int i = 0; i < strlen(mssv); i++)
  {
    buffer[i + 2] = mssv[i];
  }
  buffer[strlen(mssv) + 2] = SPECIAL_CHARACTER;
  send(clientSocket, buffer, strlen(buffer), 0);
  remove(buffer);
  if (recv(clientSocket, recvBuf, SIZE_PACKET, 0) < 0)
  {
    printf("[-]Error in receiving data.\n");
  }
  else
  {
    printf("%s>Result:<%s\n", s, s);
    detectDataSubject(recvBuf);
    remove(recvBuf);
  }
}

void detectDataSubject(char buffer[])
{
  if (buffer[0] == NO_DATA[0] && buffer[1] == NO_DATA[1])
  {
    printf("%s\n", NO_DATA);
  }
  else
  {
    int dem = 0;
    printf("SubjectID: ");
    for (int i = 0; i < strlen(buffer); i++)
    {
      if (buffer[i] == SPECIAL_CHARACTER || buffer[i] == '\n')
      {
        dem = dem + 1;
        switch (dem)
        {
        case 1:
          printf("\nSubjectName: ");
          break;
        case 2:
          printf("\nTC: ");
          break;
        case 3:
          printf("\nTC_Fee: ");
          break;
        default:
          break;
        }
      }
      else
      {
        if (dem < 4)
        {
          printf("%c", buffer[i]);
        }
        else
        {
          break;
        }
      }
    }
    printf("\n");
  }
}

void searchGrade(int clientSocket)
{
  clrscr();
  char *s = "====================";
  char buffer[SIZE_PACKET];
  char recvBuf[SIZE_PACKET];
  char mssv[LENGTH];
  char idSubject[LENGTH];
  char option = 0;
  printf("%s> Search Grade <%s", s, s);
  printf("\nEnter MSSV: ");
  fflush(stdin);
  scanf("%s", mssv);
  printf("Enter SubjectID: ");
  fflush(stdin);
  scanf("%s", idSubject);
  // if (strcmp(mssv, ALL) == 0 && strcmp(idSubject, ALL) == 0)
  // {
  //   printf("You must enter either MSSV or SubjectID diffrence 'ALL'\n");
  // }
  // else
  // {
  buffer[0] = SEARCH_GRADE;
  buffer[1] = SPECIAL_CHARACTER;
  for (int i = 0; i < strlen(mssv); i++)
  {
    buffer[i + 2] = mssv[i];
  }
  buffer[strlen(mssv) + 2] = SPECIAL_CHARACTER;
  for (int i = 0; i < strlen(idSubject); i++)
  {
    buffer[strlen(mssv) + 3 + i] = idSubject[i];
  }
  buffer[strlen(mssv) + strlen(idSubject) + 3] = SPECIAL_CHARACTER;
  send(clientSocket, buffer, strlen(buffer), 0);
  if (recv(clientSocket, recvBuf, SIZE_PACKET, 0) < 0)
  {
    printf("[-]Error in receiving data.\n");
  }
  else
  {
    printf("%s>Result:<%s\n", s, s);
    detectDataGrade(recvBuf);
    remove(recvBuf);
  }
  // }
}

void detectDataGrade(char buffer[])
{
  if (buffer[0] == NO_DATA[0] && buffer[1] == NO_DATA[1])
  {
    printf("%s\n", NO_DATA);
  }
  else
  {
    int dem = 0;
    for (int i = 0; i < strlen(buffer); i++)
    {
      if (buffer[i] == SPECIAL_CHARACTER)
      {
        dem = dem + 1;
        switch (dem)
        {
        case 2:
          printf("Score_QT: ");
          break;
        case 3:
          printf("\nScore_Final: ");
          break;
          break;
        default:
          break;
        }
      }
      else
      {
        if (dem == 3 || dem == 2)
          printf("%c", buffer[i]);
        if (dem > 3)
          break;
      }
    }
    printf("\n");
  }
}

void clrscr()
{
  system("@cls||clear");
}

int socketEventData(char *data, char hello[])
{
  int sock = 0, valread;
  struct sockaddr_in serv_addr;
  // char *hello = "Hello from client";
  char buffer[SIZE_PACKET] = {0};
  if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
  {
    printf("\n Socket creation error \n");
    return 0;
  }

  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(PORT);

  // Convert IPv4 and IPv6 addresses from text to binary form
  if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0)
  {
    printf("\nInvalid address/ Address not supported \n");
    return 0;
  }

  if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
  {
    printf("\nConnection Failed \n");
    return 0;
  }
  send(sock, hello, strlen(hello), 0);
  valread = read(sock, buffer, SIZE_PACKET);
  for (int i = 0; i < strlen(buffer); i++)
  {
    data[i] = buffer[i];
  }

  return 1;
}
