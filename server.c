// Server side C/C++ program to demonstrate Socket programming
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define PORT 4444
#define ADDRESS "127.0.0.1"
#define SIZE_PACKET 1024
#define LENGTH 30
#define SPECIAL_CHARACTER ','
#define LOG_IN '1'
#define SEARCH_STUDENT '2'
#define SEARCH_SUBJECT '3'
#define SEARCH_GRADE '4'
#define LOG_OUT '5'
#define OK "OK"
#define EXIT_SOCKET "exit"
#define NO_DATA "NO DATA DISPLAY"
#define ALL "ALL"

int analysis(char dataReq[], char *dataRes);
int checkLogin(char dataReq[]);
char *findStudent(char mssv[]);
char *findSubject(char idSubject[]);
void clrscr();

int main()
{
  clrscr();
  int sockfd, ret;
  struct sockaddr_in serverAddr;

  int newSocket;
  struct sockaddr_in newAddr;

  socklen_t addr_size;

  char buffer[SIZE_PACKET];
  char dataResponse[SIZE_PACKET];
  pid_t childpid;

  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd < 0)
  {
    printf("[-]Error in connection.\n");
    exit(1);
  }
  printf("[+]Server Socket is created.\n");

  memset(&serverAddr, '\0', sizeof(serverAddr));
  serverAddr.sin_family = AF_INET;
  serverAddr.sin_port = htons(PORT);
  serverAddr.sin_addr.s_addr = inet_addr(ADDRESS);

  ret = bind(sockfd, (struct sockaddr *)&serverAddr, sizeof(serverAddr));
  if (ret < 0)
  {
    printf("[-]Error in binding.\n");
    exit(1);
  }
  printf("[+]Bind to port %d\n", PORT);

  if (listen(sockfd, 10) == 0)
  {
    printf("[+]Listening....\n");
  }
  else
  {
    printf("[-]Error in binding.\n");
  }

  while (1)
  {
    newSocket = accept(sockfd, (struct sockaddr *)&newAddr, &addr_size);
    if (newSocket < 0)
    {
      exit(1);
    }
    printf("Connection accepted from %s:%d\n", inet_ntoa(newAddr.sin_addr), ntohs(newAddr.sin_port));

    if ((childpid = fork()) == 0)
    {
      close(sockfd);

      while (1)
      {
        remove(buffer);
        recv(newSocket, buffer, SIZE_PACKET, 0);
        if (strcmp(buffer, EXIT_SOCKET) == 0)
        {
          printf("Disconnected from %s:%d\n", inet_ntoa(newAddr.sin_addr), ntohs(newAddr.sin_port));
          break;
        }
        else
        {
          printf("\nClient - %d: %s\n",ntohs(newAddr.sin_port), buffer);
          analysis(buffer, dataResponse);
          remove(buffer);
          send(newSocket, dataResponse, strlen(dataResponse), 0);
          remove(dataResponse);
          bzero(dataResponse, sizeof(dataResponse));
          bzero(buffer, sizeof(buffer));
        }
      }
    }
  }
  close(newSocket);
  return 0;
}

int analysis(char dataReq[], char *dataRes)
{
  remove(dataRes);
  int result = 0;
  FILE *fp = fopen("./data/student.csv", "r");
  char buf[SIZE_PACKET];
  char mssv[LENGTH];

  FILE *fpSubject = fopen("./data/subject.csv", "r");
  char bufSubject[SIZE_PACKET];
  char idSubject[LENGTH];

  FILE *fpGrade = fopen("./data/grade.csv", "r");
  ;
  char bufGrade[SIZE_PACKET];
  char grade[LENGTH * 2];
  int dem = 0, demAcc = 0, demPass = 0;
  char *tempSub, *tempSV, *tempGrade;
  switch (dataReq[0])
  {
  case LOG_IN:
    result = checkLogin(dataReq);
    if (result == 1)
    {
      dataRes[0] = '1';
    }
    else
    {
      dataRes[0] = '0';
    }
    break;
  case SEARCH_STUDENT:
    result = 0;
    for (int i = 2; i < strlen(dataReq); i++)
    {
      if (dataReq[i] == SPECIAL_CHARACTER)
      {
        mssv[i - 2] = '\0';
        break;
      }
      mssv[i - 2] = dataReq[i];
    }
    for (int i = 0; i < strlen(NO_DATA); i++)
    {
      dataRes[i] = NO_DATA[i];
    }
    if (fp)
    {
      while (fgets(buf, SIZE_PACKET, fp))
      {
        for (int i = 0; i < strlen(mssv); i++)
        {
          if (mssv[i] == buf[i])
          {
            result++;
          }
          else
          {
            break;
          }
        }
        if (result >= strlen(mssv))
        {
          for (int i = 0; i < strlen(buf); i++)
          {
            dataRes[i] = buf[i];
          }
          // remove(buf);
          dataRes[strlen(buf)] = '\0';
          for (int i = strlen(buf); i < strlen(dataRes); i++)
          {
            dataRes[i] = '\0';
          }
          break;
        }
      }
      fclose(fp);
    }
    break;
  case SEARCH_SUBJECT:
    result = 0;
    for (int i = 2; i < strlen(dataReq); i++)
    {
      if (dataReq[i] == SPECIAL_CHARACTER)
      {
        idSubject[i - 2] = '\0';
        break;
      }
      idSubject[i - 2] = dataReq[i];
    }
    for (int i = 0; i < strlen(NO_DATA); i++)
    {
      dataRes[i] = NO_DATA[i];
    }
    if (fpSubject)
    {
      while (fgets(bufSubject, SIZE_PACKET, fpSubject))
      {
        for (int i = 0; i < strlen(idSubject); i++)
        {
          if (idSubject[i] == bufSubject[i])
          {
            result++;
          }
          else
          {
            break;
          }
        }
        if (result >= strlen(idSubject))
        {
          for (int i = 0; i < strlen(bufSubject); i++)
          {
            dataRes[i] = bufSubject[i];
          }
          // remove(bufSubject);
          dataRes[strlen(bufSubject)] = '\0';
          for (int i = strlen(bufSubject); i < strlen(dataRes); i++)
          {
            dataRes[i] = '\0';
          }

          break;
        }
      }
      fclose(fpSubject);
    }
    break;

  case SEARCH_GRADE:
    result = 0;
    dem = 0;
    for (int i = 0; i < strlen(NO_DATA); i++)
    {
      dataRes[i] = NO_DATA[i];
    }
    result = strlen(dataReq);
    for (int i = 2; i < result - 2; i++)
    {
      if (dataReq[i] == SPECIAL_CHARACTER)
      {
        dem++;
      }
      if (dem >= 2)
      {
        grade[i - 2] = '\0';
        break;
      }
      else
      {
        grade[i - 2] = dataReq[i];
      }
    }
    if (fpGrade)
    {
      while (fgets(bufGrade, SIZE_PACKET, fpGrade))
      {
        result = 0;
        for (int i = 0; i < strlen(grade); i++)
        {
          if (bufGrade[i] == grade[i])
          {
            result++;
          }
          else
          {
            break;
          }
        }
        if (result >= strlen(grade))
        {
          for (int i = 0; i < strlen(bufGrade); i++)
          {
            dataRes[i] = bufGrade[i];
          }
          dataRes[strlen(bufGrade)] = '\0';
          for (int i = strlen(bufGrade); i < strlen(dataRes); i++)
          {
            dataRes[i] = '\0';
          }

          break;
        }
      }
      fclose(fpGrade);
    }
    break;
  case LOG_OUT:
    dataRes[0] = OK[0];
    dataRes[1] = OK[1];
    break;
  default:
    dataRes[0] = 'A';
    break;
  }
  return 0;
}

char *findStudent(char mssv[])
{
  int result = 0;
  FILE *fp = fopen("./data/student.csv", "r");
  char buf[SIZE_PACKET];
  char *kq;
  if (fp)
  {
    while (fgets(buf, SIZE_PACKET, fp))
    {
      for (int i = 0; i < strlen(mssv); i++)
      {
        if (mssv[i] == buf[i])
        {
          result++;
        }
        else
        {
          break;
        }
      }
      if (result >= strlen(mssv))
      {
        for (int i = 0; i < strlen(buf); i++)
        {
          kq[i] = buf[i];
        }
        return kq;
      }
    }
    fclose(fp);
  }
  return NO_DATA;
}

char *findSubject(char idSubject[])
{
  int result = 0;
  FILE *fp = fopen("./data/subject.csv", "r");
  char buf[SIZE_PACKET];
  char *kq;
  if (fp)
  {
    while (fgets(buf, SIZE_PACKET, fp))
    {
      for (int i = 0; i < strlen(idSubject); i++)
      {
        if (idSubject[i] == buf[i])
        {
          result++;
        }
        else
        {
          break;
        }
      }
      if (result >= strlen(idSubject))
      {
        for (int i = 0; i < strlen(buf); i++)
        {
          kq[i] = buf[i];
        }
        return kq;
      }
    }
    fclose(fp);
  }
  return NO_DATA;
}

int checkLogin(char dataReq[])
{
  FILE *fp = fopen("./data/student.csv", "r");
  char buf[SIZE_PACKET];
  char mssv[LENGTH], password[LENGTH];
  int dem = 0, demAcc = 0, demPass = 0;
  int kq = 0;
  for (int i = 1; i < strlen(dataReq); i++)
  {
    if (dataReq[i] == SPECIAL_CHARACTER)
    {
      dem += 1;
      continue;
    }
    if (dem == 0)
    {
      mssv[demAcc++] = dataReq[i];
    }
    else if (dem == 1)
    {
      password[demPass++] = dataReq[i];
    }
    else
    {
      break;
    }
  }
  mssv[demAcc] = '\0';
  password[demPass] = '\0';
  if (!fp)
  {
    printf("Can't open file\n");
    return 0;
  }
  while (fgets(buf, SIZE_PACKET, fp))
  {
    if (buf[demAcc] == SPECIAL_CHARACTER && buf[demAcc + demPass + 1] == SPECIAL_CHARACTER)
    {
      int check = 0;
      for (int i = 0; i < demAcc; i++)
      {
        if (buf[i] != mssv[i])
        {
          break;
        }
        check++;
      }
      if (check == demAcc)
      {
        check += 1;
        for (int i = demAcc + 1; i < demAcc + demPass + 1; i++)
        {
          if (buf[i] != password[i - demAcc - 1])
          {
            break;
          }
          check++;
        }
      }
      if (check == demAcc + demPass + 1)
      {
        kq = 1;
        return kq;
      }
      else
      {
        continue;
      }
    }
  }
  fclose(fp);
  return kq;
}

void clrscr()
{
  system("@cls||clear");
}